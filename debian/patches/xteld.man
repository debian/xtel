commit 745dbd7a810d86f9eea8992943e45e30479d6d3d
Author: Samuel Thibault <samuel.thibault@ens-lyon.org>
Date:   Sat Mar 6 17:10:12 2010 +0100

    Cleanup for new upload.
    
      * debian/control:
        - New maintainer.
        - Bump Standards-Version to 3.8.4 (no changes needed).
        - Replace xbase-clients build-dep with xfonts-utils (for bdftopcf and
          mkfontdir).
        - Replace xutils with xutils-dev (for xmkmf).
        - Drop xbase-clients and xfonts-utils Depends.
        - Fix spelling.
      * xteld.man: Replace quotes with double quotes.
      * debian/prerm: Remove call to install-docs.

diff --git a/xteld.man b/xteld.man
index 8fc1b18..f2d04ab 100644
--- a/xteld.man
+++ b/xteld.man
@@ -108,7 +108,7 @@ Dans cet exemple le service, le service d'\fIAnnuaire\fP est autoris
 pendant 180 secondes (tant que c'est gratuit !). Le \fIT�l�tel3\fP est 
 accessible � tous les utilisateurs pendant 60 secondes, l'utilisateur 
 \fIuser2\fP y a droit pendant 120 secondes. Les connexions provenant de
-'linuxbox' ne sont autoris�es qu'� 'user1'. Tous les utilisateurs de 'pcwin'
+"linuxbox" ne sont autoris�es qu'� "user1". Tous les utilisateurs de "pcwin"
 ont droit au service.
 .PP
 La derni�re ligne d�finit une possibilit� de num�ro de t�l�phone 
