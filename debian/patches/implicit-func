commit 313a27fe0252af3070a3646b15071dfc8d1e723a
Author: Samuel Thibault <samuel.thibault@ens-lyon.org>
Date:   Wed Mar 13 19:52:22 2024 +0100

    Fix build with qa=+bug-implicit-func

diff --git a/Videotex/Videotex.c b/Videotex/Videotex.c
index 08c77ae..2447bbd 100644
--- a/Videotex/Videotex.c
+++ b/Videotex/Videotex.c
@@ -24,6 +24,7 @@ static char rcsid[] = "$Id: Videotex.c,v 1.28 2001/02/11 00:13:00 pierre Exp $";
  */
 #include <malloc.h>
 #include <ctype.h>
+#include <stdlib.h>
 #include <X11/IntrinsicP.h>
 #include <X11/StringDefs.h>
 #include <X11/Xos.h>
diff --git a/Videotex/VideotexP.h b/Videotex/VideotexP.h
index b125c1d..1c96657 100644
--- a/Videotex/VideotexP.h
+++ b/Videotex/VideotexP.h
@@ -374,4 +374,7 @@ typedef struct _VideotexClassRec {
 /* Class pointer. */
 extern VideotexClassRec videotexClassRec;
 
+Boolean match(int fd, char *s);
+int get_header_length(int fd);
+
 #endif /* _VideotexP_h */
diff --git a/dial.c b/dial.c
index 6b4d35c..c343a6d 100644
--- a/dial.c
+++ b/dial.c
@@ -40,10 +40,13 @@ static char rcsid[] = "$Id: dial.c,v 1.17 1998/10/02 15:00:49 pierre Exp $";
  */
 
 #include <stdio.h>
+#include <stdlib.h>
+#include <unistd.h>
 #include <sys/types.h>
 #include <sys/stat.h>
 #include <sys/time.h>
 #include <fcntl.h>
+#include <string.h>
 #ifdef SVR4
 #include <sys/mkdev.h>
 #endif /* SVR4 */
diff --git a/global.h b/global.h
index 6f21859..1e8b992 100644
--- a/global.h
+++ b/global.h
@@ -22,6 +22,8 @@
 #ifndef _global_h
 #define _global_h
 
+#include <X11/IntrinsicP.h>
+
 /*
  * pour r�cup�rer la config globale du programme
  */
diff --git a/globald.h b/globald.h
index 51477f9..9461a8a 100644
--- a/globald.h
+++ b/globald.h
@@ -83,6 +83,7 @@ void restore_tty (int);
 int do_chat (int, char*, unsigned long, int, char*, char*, int);
 void init_debug (char*);
 void close_debug (void);
+void log_err (char *s);
 #else
 
 void erreur_a_xtel();
diff --git a/ian.c b/ian.c
index c161585..37c16f7 100644
--- a/ian.c
+++ b/ian.c
@@ -23,6 +23,7 @@ static char rcsid[] = "$Id: ian.c,v 1.2 1998/10/02 15:03:46 pierre Exp $";
  * Support IAN (Eric Delaunay, delaunay@lix.polytechnique.fr)
  */
 #include <stdio.h>
+#include <string.h>
 #include "demon.h"
 #include "globald.h"
 
diff --git a/inet.c b/inet.c
index a5f8781..7e51972 100644
--- a/inet.c
+++ b/inet.c
@@ -34,6 +34,7 @@ static char rcsid[] = "$Id: inet.c,v 1.5 2001/02/13 09:36:43 pierre Exp $";
 #include <netinet/in.h>
 #include <netdb.h>
 #include <stdio.h>
+#include <strings.h>
 
 #if defined(SVR4) || defined(hpux)
 #define bcopy(src,dest,len) (memcpy(dest,src,len))
diff --git a/mdmdetect.c b/mdmdetect.c
index a0d1799..2a65fdf 100644
--- a/mdmdetect.c
+++ b/mdmdetect.c
@@ -47,6 +47,9 @@ static char rcsid[] = "$Id: mdmdetect.c,v 1.3 2001/02/11 00:02:58 pierre Exp $";
 #include <sys/mkdev.h>
 #endif /* SVR4 */
 
+#include "demon.h"
+#include "globald.h"
+
 #define TIMEOUT_READ		5
 #define TEMPO			1000000
 
diff --git a/modem.c b/modem.c
index 6c067b8..4760ae3 100644
--- a/modem.c
+++ b/modem.c
@@ -24,10 +24,12 @@ static char rcsid[] = "$Id: modem.c,v 1.2 2001/02/13 09:39:23 pierre Exp $";
 
 #include <stdio.h>
 #include <stdlib.h>
+#include <unistd.h>
 #include <string.h>
 #include <errno.h>
 #include <sys/types.h>
 #include <sys/time.h>
+#include <ctype.h>
 #ifdef NO_TERMIO
 #include <sgtty.h>
 #else
diff --git a/protocoles.c b/protocoles.c
index 450c4d9..5bd7915 100644
--- a/protocoles.c
+++ b/protocoles.c
@@ -24,6 +24,7 @@ static char rcsid[] = "$Id: protocoles.c,v 1.1 1996/09/14 22:10:31 pierre Exp $"
  */
 #include "xtel.h"
 #include <signal.h>
+#include <sys/wait.h>
 #include <string.h>
 
 static int pid_tele;
diff --git a/teleinfo.c b/teleinfo.c
index 3b11f48..d2ca46a 100644
--- a/teleinfo.c
+++ b/teleinfo.c
@@ -27,6 +27,7 @@ static char rcsid[] = "$Id: teleinfo.c,v 1.6 2001/02/11 00:03:58 pierre Exp $";
 #include "xtel.h"
 
 #include <signal.h>
+#include <sys/wait.h>
 #include <fcntl.h>
 #ifndef SVR4
 #include <sys/types.h>
diff --git a/xteld.c b/xteld.c
index e5a7f96..ff6f700 100644
--- a/xteld.c
+++ b/xteld.c
@@ -61,8 +61,10 @@ static char rcsid[] = "$Id: xteld.c,v 1.33 2001/02/13 09:40:49 pierre Exp $";
 #include <sys/stat.h>
 #include <sys/time.h>
 #include <sys/param.h>
+#include <unistd.h>
 #include <time.h>
 #include <signal.h>
+#include <sys/wait.h>
 #include <string.h>
 #include <fcntl.h>
 
